#include <iostream>
#include <cmath>
#include <array>
#include <vector>
#include <random>

using int_Vector = std::vector <int>;
using float_Vector = std::vector <float>;
using int_Matrix = std::vector < std::vector<int> >;



/* Notatki:

// Tablice statyczne:
#include <array>


// Tablica 3 liczb całkowitych o określonych wartościach:
std::array <int, 3> arr {1,2,3};


// Pusta tablica 5 liczb zmiennoprzecinkowych:
std::array <float, 5> arr2;


// Dostęp do określonego elementu (po indeksie, nie numerze):
arr[i];

lub

arr.at(i);    <-- Zalecany sposób


// Wielkość kolekcji (liczba elementów):
arr.size();






// Wektory (tablice dynamiczne):
#include <vector>


// Inicjalizacja (rzeczy oznaczone slashem są opcjonalne):
std::vector <typ> nazwa /(rozmiar poczatkowy)/;


// Dodawanie nowej wartości na końcu wektora:
v.push_back(x);


// usuwanie określonego elementu (po indeksie):
v.erase(v1.begin() + i);


// czyszczenie wektora (usuwanie wszystkich elementów)
v.clear();


// Zmiana rozmiaru wektora (i wypełnienie nowych elementów wartościami domyślnymi):
v.resize(20);


// Zmiana rozmiaru wektora i wypełnienie nowych elementów określoną wartością:
v.resize(20, 1337);


// Reszta jak w tablicach statycznych (dostęp do danego elementu, rozmiar kolekcji itd.)






// Pętla range-based for (iterowanie po wszystkich elementach danej kolekcji):
for (auto e : v) {

cout << e; // wyswietla kolejne elementy kolekcji

}






// Aliasy typów:
using Matrix = std::vector<std::vector<int>>;
using Vector = std::vector<int>;



*/



void AbyKontynuowac() {

    std::cout << "\n\n\nAby kontynuowac, wprowadz \";\" (srednik).\n\n";

    char c = 'a';

    do {
        c = getchar();
    } while (c != ';');

    std::cout << "\n\n";
}



/* Zadanie 1. */

void zadanie_1() {

    std::cout << "\n\nZadanie 1.\n\n";



    std::array <int, 100> arr1;



    for (int i=1; i<=100; ++i) {

        arr1[i-1] = 100+i-1;

    }



    unsigned int suma=0;

    for (auto e : arr1) {

        suma += e;

    }



    std::cout << suma;



    AbyKontynuowac();

}



/* Zadanie 2. */

void printIntVector(int_Vector v) {

    for (auto e : v) {

        std::cout << e << "\n";

    }

}

void zadanie_2() {

    std::cout << "\n\nZadanie 2.\n\n"
                 "Test implementacji funkcji \"print(vector)\":\n\n";


    int_Vector v1 {1,2,3,4};

    printIntVector(v1);


    std::cout << "\n";


    v1.push_back(5);

    printIntVector(v1);


    std::cout << "\n";


    v1.erase(v1.begin() + 2);

    printIntVector(v1);


    std::cout << "\n" << v1.size() << "\n\n";


    v1.clear();


    v1.resize(20, 2137);

    printIntVector(v1);


    AbyKontynuowac();

}

/* Wszystko działa jak należy */



/* Zadanie 3. */

float sumFloatVector(float_Vector v) {

    float suma=0;

    for (auto e : v) {

        suma += e;

    }

    return suma;

}

float averageFloatVector(float_Vector v) {

    return ( sumFloatVector(v) / v.size() );

}

std::array <float, 2> MinMaxFloatVector (float_Vector v) {

    float min=v[0], max=v[0];

    std::array <float, 2> arr = {0,0};

    for (int i=1; i < v.size(); ++i) {

        if (v[i] > max) {

            max = v[i];

        }

        else if (v[i] < min) {

            min = v[i];

        }

    }

    arr[0] = min;
    arr[1] = max;

    return arr;

}

void zadanie_3() {

    unsigned int n=0;

    std::cout << "\n\nZadanie 3.\n\n"
                 "Podaj ilosc elementow Twojego wektora:\n\n";
    std::cin >> n;


    float_Vector v1;

    for (int i=1; i <= n; ++i) {

        float temp1;

        std::cout << "\n\nPodaj " << i << ". element wektora:\n";
        std::cin >> temp1;

        v1.push_back(temp1);

    }


    std::cout << "\n\nOto statystyki dla Twojego wektora:\n\n"
                 "Suma wszystkich elementow: " << sumFloatVector(v1) << "\n"
                 "Srednia arytmetyczna ze wszystkich elementow: " << averageFloatVector(v1) << "\n"
                 "Najmniejszy element: " << MinMaxFloatVector(v1)[0] << "\n"
                 "Najwiekszy element: " << MinMaxFloatVector(v1)[1];

    AbyKontynuowac();

}



/* Zadanie 4. */

/*
    Alternatywny sposób na liczby pseudolosowe. Tak, wiem że archaiczny i za mało "losowy". Nawet CLion mi to powiedział xP
*/

/*
    #include <cstdlib>
    #include <ctime>

    std::srand (std::time(NULL));

    int randomInt (int min, int max) {

    return (rand() % (max - min)) + min;

}*/

int randomInt(int min, int max) {
    static std::default_random_engine e{};
    std::uniform_int_distribution <int> d(min, max);
    return d(e);
}

/*
    Listing z PDFa ma te polecenia bez prefixów "std::", dlatego program na początku mi nie działał. Zalecam to poprawić :)
*/

int_Vector randomVector (unsigned int size, int min, int max) {

    int_Vector v2 (size);

    for (int i=0; i < size; ++i) {

        v2[i] = randomInt(min, max);

    }

    return v2;

}

void zadanie_4() {

    unsigned int size1=0;
    int min1=0, max1=0;

    std::cout << "\n\nZadanie 4.\n\n"
                 "Podaj dlugosc wektora do wygenerowania:\n\n";
    std::cin >> size1;

    std::cout << "\n\n Podaj minimalna wartosc elementu wektora do wygenerowania (calkowita):\n\n";
    std::cin >> min1;

    std::cout << "\n\n Podaj maksymalna wartosc elementu wektora do wygenerowania (calkowita):\n\n";
    std::cin >> max1;

    std::cout << "\n\nOto elementy Twojego wektora:\n\n";

    printIntVector( randomVector(size1, min1, max1) );

    AbyKontynuowac();

}



/* Zadanie 5. */

int_Vector int_scalarProductVector (int_Vector v, int_Vector u) {

    int_Vector w ( v.size() );

    for (int i=0; i < w.size(); ++i) {

        w[i] = v[i]*u[i];

    }

    return w;

}

int sumIntVector(int_Vector v) {

    int suma=0;

    for (auto e : v) {

        suma += e;

    }

    return suma;

}

void zadanie_5() {

    unsigned int n=0;

    std::cout << "\n\nZadanie 5.\n\n"
                 "Podaj wielkosc wektorow, ktorych iloczyn skalarny chcesz obliczyc:\n\n";
    std::cin >> n;


    int_Vector v1 = randomVector(n, 3, 27);
    int_Vector v2 = randomVector(n, 3, 27);


    std::cout << "\n\nOto oba wektory (od gory do dolu):\n\n";
    printIntVector(v1);
    std::cout << "\n";
    printIntVector(v2);


    int_Vector v_sp = int_scalarProductVector(v1, v2);
    std::cout << "\n\nOto wektor ze skladnikami ich iloczynu skalarnego (dla kazdej pary odpowiadajacych sobie elementow osobny skladnik):\n\n";
    printIntVector(v_sp);


    std::cout << "\n\nA oto ich kompletny iloczyn skalarny:\n\n" << sumIntVector(v_sp);


    AbyKontynuowac();

}



/* Notatki o stringach:



 std::getline (std::cin, str);


 + żeby getline dobrze działał, należy przed nim umieścić polecenie "cin.ignore()", a najlepiej dołączyć bibliotekę "<limits>"
 i umieścić rozwinięte cin.ignore(), w postaci: "cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');".


 std::to_string(liczba);

 std::stoi(str); albo str.stoi(); (?)
 std::stof(str) / str.stof() (?)
 std::stoui(str) / str.stoui() (?)
 std::stoul(str) / str.stoul() (?)

 str.find(str2, /init_pos/);
 str.rfind(str2, /init_pos/);

 str1.swap(str2);

 str3 = str1 + str2;
 str1 += str2;

 str1.substr(pos, count);

 str.front();
 str.back();

 str.empty();
 str.size();

 str.insert (pos, str);
 str.erase (pos, count);
 str.clear();
 str.push_back(ch);
 str.pop_back();
 str.replace(pos, count, str2);

 std::isalnum(ch);
 std::isalpha(ch);
 std::islower(ch);
 std::isupper(ch);
 std::isdigit(ch);
 std::isspace(ch);
 std::isgraph(ch);

 std::tolower(ch);
 std::toupper(ch);



*/


/* Zadanie 6. */

std::string allToUpper (std::string str) {

    for (int i=0; i < str.size(); ++i) {

        if (str[i] >= 97 && str[i] <= 122) {

            str[i] -= 32;
        }
    }

    return str;
}

std::string allToLower (std::string str) {

    for (int i=0; i < str.size(); ++i) {

        if (str[i] >= 65 && str[i] <= 90) {

            str[i] += 32;
        }
    }

    return str;
}

std::string allToOpposite (std::string str) {

    for (int i=0; i < str.size(); ++i) {

        if (str[i] >= 97 && str[i] <= 122) {

            str[i] -= 32;
        }

        else if (str[i] >= 65 && str[i] <= 90) {

            str[i] += 32;
        }
    }

    return str;
}

void zadanie_6() {

    std::cout << "\n\nZadanie 6.\n\n"
                 "Podaj jakas linie tekstu (mozesz wpisac co chcesz, byle bez polskich znakow i innych spoza tabeli ASCII):\n\n";

    std::string str1="";

    std::cin.ignore();
    std::getline(std::cin, str1);



/*
    Moje próby na Null-terminated byte stringach, ale nie działa, nawet jak zrobię wszystko jak w cppreference (dołączenie
    biblioteki clocale, odpowiednich poleceń, rzutowanie na unsigned char itd.). Generalnie dużo pieprzenia i brak efektu.
    IMAO lepiej robić to metodą prymitywniejszą, ale przynajmniej działającą XD
*/

/*
    Biblioteki do dołączenia:
    #include <cctype>
    #include <clocale>



    Funkcje do wklejenia przed funkcją "zadanie_6()":

    bool my_isupper (char ch) {
        return std::isupper(static_cast<unsigned char>(ch));
    }

    bool my_islower (char ch) {
        return std::islower(static_cast<unsigned char>(ch));
    }

    char my_toupper (char ch) {
        return static_cast<char>(std::toupper(static_cast<unsigned char>(ch)));
    }

    char my_tolower (char ch) {
        return static_cast<char>(std::tolower(static_cast<unsigned char>(ch)));
    }



    Treść kodu wpleciona do programu dokładnie w miejscu, w którym zaczyna się ten komentarz:

    std::setlocale(LC_ALL, "en_US.iso88591");


    for (int i=0; i < str2.size(); ++i) {

        if (my_islower(str2[i])) {
            my_toupper(str2[i]);
        }
    }


    for (int i=0; i < str3.size(); ++i) {

        if (my_isupper(str3[i])) {
            my_tolower(str3[i]);
        }
    }


    for (int i=0; i < str1.size(); ++i) {

        if (my_islower(str1[i])) {
            my_toupper(str1[i]);
        }

        else if (my_isupper(str1[i])) {
            my_tolower(str1[i]);
        }
    }
*/



    std::cout << "\n\na) " << allToUpper(str1) <<
                 "\n\nb) " << allToLower(str1) <<
                 "\n\nc) " << allToOpposite(str1);

    AbyKontynuowac();
}



/* Zadanie 7. */

std::string invertString (std::string str) {

    std::string outputString;
    outputString.resize(str.size());

    for (int i=0; i < str.size(); ++i) {

        outputString[i] = str[str.size()-1-i];
    }

    return outputString;
}

std::string purifyString (std::string str) {

    for (int i=0; i < str.size(); ++i) {

        if (str[i] <= 32 || str[i] == 127) {

            str.erase(i, 1);
            --i;
        }
    }

    return str;
}

bool czyPalindrom (std::string str) {

    str = allToLower( purifyString(str) );

    return (str == invertString(str));
}

void zadanie_7() {

    std::cout << "\n\nZadanie 7.\n\n"
                 "Podaj tekst do 30 znakow:\n\n";

    std::string str1="";

    bool stop_01 = false;
    while (!stop_01) {

        std::cin.ignore();
        std::getline(std::cin, str1);

        if (str1.size() > 30) {

            std::cout << "\n\nZa dlugi tekst. Podaj do 30 znakow:\n\n";
        }

        else { stop_01 = true; }
    }

    std::cout << "\n\nOto odwrocony tekst:\n\n"
              << invertString(str1) << "\n\n\n"
                 "A to oczyszczony, ujednolicony pod wzgledem wielkosci liter i odwrocony:\n\n"
              << invertString(allToLower(purifyString(str1)));

    std::cout << "\n\n\n";

    if (czyPalindrom(str1)) {

        std::cout << "Podany przez Ciebie tekst jest palindromem ";

        if (invertString(allToLower(purifyString(str1))).size() % 2 == 0) {

            std::cout << "parzystym.";
        }

        else {

            std::cout << "nieparzystym.";
        }
    }

    else {

        std::cout << "Podany przez Ciebie tekst nie jest palindromem.";
    }

    AbyKontynuowac();
}



/* Zadanie 8. */

int_Matrix createMatrix (std::array <unsigned int, 2> shape) {

    int_Matrix Matrix;

    Matrix.resize(shape[0]);

    for (int i=0; i < shape[0]; ++i) {

        Matrix[i].resize(shape[1], 0);
    }

    return Matrix;
}

int_Matrix randomMatrix (std::array <unsigned int, 2> shape, int min, int max) {

    int_Matrix Matrix;


    Matrix.resize(shape[0]);


    for (int i=0; i < shape[0]; ++i) {

        Matrix[i].resize(shape[1]);

        for (int j=0; j < shape[1]; ++j) {

            Matrix[i][j] = randomInt(min, max);
        }
    }

    return Matrix;
}

void printMatrix (int_Matrix Matrix) {

    std::cout << "\n";

    for (int i=0; i < Matrix.size(); ++i) {

        std::cout << "\n\n\t";

        for (int j=0; j < Matrix[i].size(); ++j) {

                std::cout << Matrix[i][j];

                if ( j+1 < Matrix[i].size() ) {

                    std::cout << "\t";
                }
        }
    }

    std::cout << "\n\n";
}

void zadanie_8() {

    std::cout << "\n\nZadanie 8.\n\n";

    unsigned int m = 5, n = 4;
    int_Matrix A = createMatrix({m, n});
    printMatrix(A);

    int_Matrix B = randomMatrix({m, n}, 1, 5);
    printMatrix(B);

    AbyKontynuowac();
}



/* Zadanie 9.

    Uważam, że stosowanie aliasów to świetna sprawa, czyniąca kod znacznie czytelniejszym (o ile ktoś
    uprzednio zapoznał się z ich deklaracjami na początku pliku).

*/



int main() {

    bool stop_menu = false;
    while (!stop_menu) {

        char wybor = 'a';

        std::cout << "\n\nWitaj w przegladzie zadan z laboratorium 4.! Lista opcji:\n\n"
                "\t\"1\" - Zadanie 1. (Suma elementow tablicy statycznej)\n"
                "\t\"2\" - Zadanie 2. (Wyswietlanie wektora)\n"
                "\t\"3\" - Zadanie 3. (Suma, srednia i min-max wektora)\n"
                "\t\"4\" - Zadanie 4. (Losowy wektor)\n"
                "\t\"5\" - Zadanie 5. (Iloczyn skalarny dwoch wektorow)\n"
                "\t\"6\" - Zadanie 6. (Zamiana wielkosci liter w napisie)\n"
                "\t\"7\" - Zadanie 7. (Sprawdzanie, czy wczytany z klawiatury napis to palindrom)\n"
                "\t\"8\" - Zadanie 8. (Tworzenie, generowanie i wyswietlanie macierzy)\n"
                "\t\";\" (srednik) - Wyjdz z programu.\n\n";

        std::cin >> wybor;
        switch (wybor) {
            case '1': {zadanie_1(); break;}
            case '2': {zadanie_2(); break;}
            case '3': {zadanie_3(); break;}
            case '4': {zadanie_4(); break;}
            case '5': {zadanie_5(); break;}
            case '6': {zadanie_6(); break;}
            case '7': {zadanie_7(); break;}
            case '8': {zadanie_8(); break;}
            case ';': {stop_menu = true; break;}
            default: {std::cout << "\n\nNie wybrano zadnej opcji. Restartuje..."; break;}
        }
    }

    return 0;
}