#include <iostream>
#include <cmath>

const float Pi = 3.1415;

void AbyKontynuowac() {

    std::cout << "\n\nAby kontynuowac, wprowadz \";\" (srednik).\n\n";

    char c = 'a';

    do {
        c = getchar();
    } while (c != ';');

    std::cout << "\n";

}



/* Zadanie 1. */

bool triangleExistence (float a, float b, float c) {
    return (a + b > c) && (b + c > a) && (a + c > b);               // CLion mi to podpowiedział. Normalnie zrobiłbym ifem i return true / falsem xP
}

float triangleArea (float a, float b, float c) {
    float p = (a+b+c) / 2;
    return sqrtf( p*(p-a)*(p-b)*(p-c) );
}

void zadanie_1() {

    std::cout << "\n\nZadanie 1.\n\n";



    bool stop_00 = false;
    while (!stop_00) {

        std::cout << "\n\nWitaj w kalkulatorze pola trojkata!\n\n"
                     "Jesli chcesz stad wyjsc, wpisz \";\" (srednik).\n"
                     "Jesli chcesz rozpoczac prace kalkulatora, wpisz \"/\" (slash).\n\n";

        char czyZaczac = 'a';

        bool stop_05 = false;
        while (!stop_05) {

            std::cin >> czyZaczac;

            if (czyZaczac == ';' || czyZaczac == '/') {

                stop_05 = true;

            }

            else {

                std::cout << "\n\nNieprawidlowy znak sterujacy\n"
                             "Jesli chcesz stad wyjsc, wpisz \";\" (srednik).\n"
                             "Jesli chcesz rozpoczac prace kalkulatora, wpisz \"/\" (slash).\n\n";

            }

        }



        if (czyZaczac == ';') {

            break;

        }



        float a = 0, b = 0, c = 0;



        std::cout << "\n\nPodaj kolejno dlugosci bokow trojkata.\n\nPierwszy:\n";



        bool stop_01 = false;
        while (!stop_01) {

            std::cin >> a;

            if (a <= 0) {

                std::cout << "\n\nNiedodatnia wartosc? Oj Ty smieszku... Podaj poprawna dlugosc pierwszego boku:\n";

            } else { stop_01 = true; }

        }



        std::cout << "\n\nDrugi:\n";



        bool stop_02 = false;
        while (!stop_02) {

            std::cin >> b;

            if (b <= 0) {

                std::cout << "\n\nNiedodatnia wartosc? Oj Ty smieszku... Podaj poprawna dlugosc drugiego boku:\n";

            } else { stop_02 = true; }

        }



        std::cout << "\n\nTrzeci:\n";



        bool stop_03 = false;
        while (!stop_03) {

            std::cin >> c;

            if (c <= 0) {

                std::cout << "\n\nNiedodatnia wartosc? Oj Ty smieszku... Podaj poprawna dlugosc trzeciego boku:\n";

            } else { stop_03 = true; }

        }



        if ( triangleExistence(a, b, c) ) {

            std::cout << "\n\nPole trojkata wynosi: " << triangleArea(a, b, c);

        }

        else {

            std::cout << "\n\nZ bokow podanej dlugosci nie da sie zbudowac trojkata.";

        }



        std::cout << "\n\nJesli chcesz zaczac od nowa, wpisz \"/\" (slash).\n"
                     "Jesli chcesz wyjsc z kalkulatora, wpisz \";\" (srednik).\n\n";



        char dieAntwoord = 'a';



        bool stop_04 = false;
        while (!stop_04) {

            std::cin >> dieAntwoord;

            if (dieAntwoord == ';') {

                stop_00 = true;
                stop_04 = true;

            }

            else if (dieAntwoord == '/') {

                stop_04 = true;

            }

            else {

                std::cout << "\n\nNieprawidlowy znak sterujacy.\n"
                             "Jesli chcesz zaczac od nowa, wpisz \"/\" (slash).\n"
                             "Jesli chcesz wyjsc z kalkulatora, wpisz \";\" (srednik).\n\n";

            }

        }

    }

}



/* Zadanie 2. */

float toDegrees (float rad) {
    return (rad*180)/Pi;
}

void zadanie_2() {

    std::cout << "\n\nZadanie 2.\n\n";



    std::cout << "Witaj w przeliczniku radianow na stopnie!\n\n"
                 "Podaj kat do zamiany (w radianach):\n\n";



    float kat_rad = 0;



    std::cin >> kat_rad;



    std::cout << "\n\n\t" << kat_rad << " (rad) = " << toDegrees (kat_rad) << " (deg)";



    AbyKontynuowac();

}



/* Zadanie 3. */

float calculateAngle (float c, float a, float b) {

    float cos = (a*a + b*b - c*c) / (2*a*b);

    return toDegrees(acosf(cos));

}

/*
   Jak nie połączyłem mianownika nawiasem, to wywalało domain error.
   Cóż... widocznie C++ nie umie w kolejność działań :/
*/

void zadanie_3() {

    std::cout << "\n\nZadanie 3.\n\n";

    float a=0, b=0, c=0;

    std::cout << "Witaj w kalkulatorze katow trojkata o bokach podanej dlugosci!\n"
                 "Podaj dlugosc pierwszego boku (a):\n\n";

    std::cin >> a;

    std::cout << "\n\nTeraz drugiego (b):\n\n";
    std::cin >> b;

    std::cout << "\n\n...i trzeciego (c):\n\n";
    std::cin >> c;

    if (triangleExistence(a,b,c)) {

        std::cout << "\n\nMiary katow wynosza:\n"
                  << "Kat naprzeciw boku \"a\" = " << calculateAngle(a, b, c) << "\n"
                  << "Kat naprzeciw boku \"b\" = " << calculateAngle(b, a, c) << "\n"
                  << "Kat naprzeciw boku \"c\" = " << calculateAngle(c, a, b);

    }

    else {

        std::cout << "\n\nZ bokow podanej dlugosci nie da sie zbudowac trojkata. Odpal kalkulator jeszcze raz.";

    }

    AbyKontynuowac();

}



/* Zadanie 4. */

unsigned long long recursiveFactorial (int n) {

    if (n == 0) {

        return 1;

    }

    else {

        return ( n * recursiveFactorial(n-1) );

    }

}

unsigned long long iterativeFactorial (int n) {

    unsigned long long wynik = 1;

    while (n >= 1) {

        wynik *= n;

        --n;

    }

        return wynik;

}

void zadanie_4() {

    std::cout << "\n\nZadanie 4.\n\n";

    std::cout << "Witaj w kalkulatorze silni za pomoca rekurencji i iteracji!\n"
                 "Podaj liczbe naturalna, dla ktorej chcesz obliczyc silnie:\n\n";

    int n = 0;

    std::cin >> n;



    std::cout << "\n\nSilnia metoda rekurencyjna: " << n << "! = " << recursiveFactorial(n);

    AbyKontynuowac();

    std::cout << "\n\nSilnia metoda iteracyjna: " << n << "! = " << iterativeFactorial(n);

    AbyKontynuowac();

}

/*
   Używając unsigned long [int] jako zwracanego typu, maksymalnym argumentem dla obu silni bylo "33". Po podaniu arg. "34"
   zwracane było 0 (zero).

   Używając unsigned long long [int], maksymalnym argumentem było "65".

   Nie zauważyłem różnicy w czasie działania obydwu funkcji. Niezależnie od argumentu, zwracały wartości natychmiast.
   Może to kwestia sprzętu, bo mam i7 2600k @3.40 GHz, 16 GB RAMu i fabrycznie podkręconego GTXa 680 XD
*/



/* Zadanie 5. */

unsigned long long recursiveFibonnacci (int n) {

    if (n == 1) {

        return 0;

    }

    else if (n == 2) {

        return 1;

    }

    else return recursiveFibonnacci(n-1) + recursiveFibonnacci(n-2);

}

void zadanie_5() {

    std::cout << "\n\nZadanie 5.\n\n";

    std::cout << "Witaj w programie wypisujacym wszystkie wyrazy ciagu Fibonnacciego mniejsze od 300!\n"
                 "Na Twoj znak zostana one wyswietlone.";

    AbyKontynuowac();

    int n = 1;

    while (recursiveFibonnacci(n) < 300) {

        std::cout << n << ": " << recursiveFibonnacci(n) << "\n";

        ++n;

    }

    AbyKontynuowac();

}



/* Zadanie 6. */

unsigned long long iterativeFibonnacci (int n) {

    unsigned long long iterFib[n] = {0};

    iterFib[0] = 0;
    iterFib[1] = 1;

    for (int k=3; k <= n; ++k) {

        iterFib[k-1] = iterFib[k-2] + iterFib [k-3];

    }

    return iterFib[n-1];

}

void zadanie_6() {

    std::cout << "\n\nZadanie 6.\n\n";

    int n=0;

    std::cout << "Witaj w programie porownawczym dla 2 funkcji wyznaczajacych n-ty wyraz ciagu Fibonnacciego!\n"
                 "Podaj nr wyrazu, ktory chcesz wyznaczyc:\n\n";

    std::cin >> n;

    std::cout << "\n\nMetoda rekurencyjna: " << recursiveFibonnacci(n);

    AbyKontynuowac();

    std::cout << "Metoda iteracyjna: " << iterativeFibonnacci(n);

    AbyKontynuowac();

}

/*
   Różnicę w czasie wyznaczania najlepiej widać dla argumentu "45" (przynajmniej w przypadku mojego sprzętu).
   Dla rekurencji wynik otrzymałem po ok. 6 sekundach, podczas gdy dla iteracji, niemal natychmiast.
   Z tego co pamiętam z liceum, iteracja trwa szybciej, bo nie musi wspomagać się stosem (nie wymaga tylu operacji w obszarze pamięci).
   Wolę się jednak upewnić i o tym jeszcze poczytać albo zapytać kogoś kompetentnego, np. Pana :)
*/



int main() {

    bool stop_menu = false;
    while (!stop_menu) {

        char wybor = 'a';

        std::cout << "\n\nWitaj w przegladzie zadan z laboratorium nr 3! Lista opcji:\n\n"
                "\t\"1\" - Zadanie 1.\n"
                "\t\"2\" - Zadanie 2.\n"
                "\t\"3\" - Zadanie 3.\n"
                "\t\"4\" - Zadanie 4.\n"
                "\t\"5\" - Zadanie 5.\n"
                "\t\"6\" - Zadanie 6.\n"
                "\t\";\" (srednik) - Wyjdz z programu.\n\n\t";

        std::cin >> wybor;
        switch (wybor) {
            case '1': {zadanie_1(); break;}
            case '2': {zadanie_2(); break;}
            case '3': {zadanie_3(); break;}
            case '4': {zadanie_4(); break;}
            case '5': {zadanie_5(); break;}
            case '6': {zadanie_6(); break;}
            case ';': {stop_menu = true; break;}
            default: {std::cout << "\n\nNie wybrano zadnej opcji. Restartuje..."; break;}
        }
    }

    return 0;
}