#include <iostream>
#include <cmath>
#include <array>
#include <vector>
#include <random>
#include <fstream>

using int_Vector = std::vector <int>;
using float_Vector = std::vector <float>;
using int_Matrix = std::vector < std::vector<int> >;
using float_Matrix = std::vector < std::vector<float> >;



void AbyKontynuowac() {

    std::cout << "\n\n\nAby kontynuowac, wprowadz \";\" (srednik).\n\n";

    char c;

    do {
        c = getchar();
    } while (c != ';');

    std::cout << "\n\n";
}



/* Zadanie 1. */

void printIntVector_vertical(const int_Vector& v) {

    for (auto e : v) {

        std::cout << e << "\n";
    }
}

void printIntVector_horizontal(const int_Vector& v) {

    for (auto e : v) {

        std::cout << e << "\t";
    }
}

//          Funkcja nie modyfikuje zawartości wektora, czyli można go przekazać przez stałą referencję


float sumFloatVector(const float_Vector& v) {

    float suma=0;

    for (auto e : v) {

        suma += e;

    }

    return suma;
}

//          j.w. (zawartość wektora tylko do odczytu)


float averageFloatVector(const float_Vector& v) {

    return ( sumFloatVector(v) / v.size() );
}

//          j.w. (funkcje składowe nie modyfikują zawartości wektora)


std::array <float, 2> MinMaxFloatVector (const float_Vector& v) {

    float min=v[0], max=v[0];

    std::array <float, 2> arr = {0,0};

    for (int i=1; i < v.size(); ++i) {

        if (v[i] > max) {

            max = v[i];

        } else if (v[i] < min) {

            min = v[i];
        }
    }

    arr[0] = min;
    arr[1] = max;

    return arr;
}

//          j.w. (zawartość wektora tylko do odczytu)



/* Zadanie 2. */

int randomInt(const int& min, const int& max) {
    static std::default_random_engine e{};
    std::uniform_int_distribution <int> d(min, max);
    return d(e);
}

int_Vector randomIntVector (const unsigned int& size, const int& min, const int& max) {

    int_Vector v2 (size);

    for (int i=0; i < size; ++i) {

        v2[i] = randomInt(min, max);
    }

    return v2;
}

bool areEqual_2Vectors (const int_Vector& v1, const int_Vector& v2) {

    if ( v1.size() != v2.size() ) {return false;}

    bool cond1 = true;

    for (int i=0; i < v1.size(); ++i) {

        if ( v1[i] != v2[i] ) {

            cond1 = false;
            break;
        }
    }

    return cond1;
}

//          Zawartości wektorów tylko do odczytu, czyli powinno się je przekazać przez stałe referencje


int_Vector add_2Vectors (int_Vector v1, const int_Vector& v2) {

    for (int i=0; i < v2.size(); ++i) {

        if (i < v1.size()) {

            v1[i] += v2[i];

        } else {

            v1.push_back(v2[i]);
        }
    }

    return v1;
}

/*
  Pierwszy wektor wykorzystuję do zwrócenia wynikowego (i tak musiałbym utworzyć nowy wektor wewnątrz funkcji,
  gdybym oba wejściowe przekazał przez stałe referencje). Drugi jest mi potrzebny jedynie do odczytu jego zawartości
  i użycia jej do modyfikacji pierwszego.
*/


int_Vector subtract_2Vectors (int_Vector v1, const int_Vector& v2) {

    for (int i=0; i < v2.size(); ++i) {

        if (i < v1.size()) {

            v1[i] -= v2[i];

        } else {

            v1.push_back(-v2[i]);
        }
    }

    return v1;
}

//          j.w.

int_Vector multiply_VectorByScalar (int_Vector v1, const int& scalar) {

    for (int i=0; i < v1.size(); ++i) {

        v1[i] *= scalar;
    }

    return v1;
}

/*
    Znów używam pierwszego wektora do zwrócenia wynikowego. Skalar pobieram przez stałą referencję, bo nie jest
    modyfikowany podczas działania funkcji.
*/


void zadanie_2() {

    std::cout << "\n\nZadanie 2.\n\n\n";


    int_Vector v1 = randomIntVector(5, 1, 5);
    std::cout << "Test porownania wektora z samym soba:\n\n"
              << areEqual_2Vectors(v1, v1) << "\n\n\n";


    int_Vector v2 = randomIntVector(5, 1, 5);
    std::cout << "Test porownania 2 wektorow:\n\n"
              << areEqual_2Vectors(v1, v2) << "\n\n\n";


    std::cout << "Suma i roznica dwoch wektorow:" << "\n\n";

    v2.push_back(4);

    std::cout << "v1:\t\t";
    printIntVector_horizontal(v1);

    std::cout << "\n\nv2:\t\t";
    printIntVector_horizontal(v2);

    std::cout << "\n\nSuma:\t\t";
    printIntVector_horizontal(add_2Vectors(v1, v2));

    std::cout << "\n\nRoznica:\t";
    printIntVector_horizontal(subtract_2Vectors(v1, v2));


    int skalar=1;
    std::cout << "\n\nPodaj skalar (l. calkowita), przez ktory chcialbys przemnozyc losowy, 5-elementowy wektor v1:\n\n";
    std::cin >> skalar;
    std::cout << "\n\n";
    printIntVector_horizontal(multiply_VectorByScalar(v1, skalar));


    AbyKontynuowac();
}



/* Zadanie 3. */

std::fstream plik;

void writeVector (const int_Vector& vec, const std::string& fileDir) {

    plik.open(fileDir, std::ios::out | std::ios::trunc);

    if ( plik.is_open() && plik.good() ) {

        for (int i = 0; i + 1 < vec.size(); ++i) {

            plik << vec[i] << "\t";
        }

        plik << vec[vec.size() - 1];
        plik.close();
        std::cout << "\n\nOtworz plik w notatniku i sprawdz, czy wszystko gra i trabi :)";

    } else {

        plik.close();
        std::cout << "\n\nNie udalo sie otworzyc pliku... Zrestartuj program i sprobuj ponownie.";
    }
}

void zadanie_3() {

    std::cout << "\n\nZadanie 3.\n\n";

    int_Vector v1 = randomIntVector(100, -5, 5);
    std::string temp_dir = "";

    std::cout << "Podaj sciezke do pliku tekstowego, w ktorym chcesz zapisac losowy, 100-elementowy wektor liczb calkowitych,"
                 " o wartosci elementow od -5 do 5:\n\n";
    std::cin.ignore();
    std::getline(std::cin, temp_dir);

    writeVector(v1, temp_dir);

    AbyKontynuowac();
}

/*
    Wiedząc, że jest to zapis wektora liczb całkowitych, mogę jednoznacznie odczytać zawartość pliku.
    W innym razie, nie wiedziałbym co to jest. Wniosek: dokumentacja jest ważna :P
*/



/* Zadanie 4. */

int_Vector readIntVector (const std::string& fileDir) {

    plik.open(fileDir, std::ios::in);
    int_Vector v (0);

    if ( plik.is_open() && plik.good() ) {

        int temp = 0;

        while ( !plik.eof() ) {

            plik >> temp;
            v.push_back(temp);
        }

        plik.close();
        return v;

    } else {

        plik.close();
        std::cout << "\n\nNie udalo sie otworzyc pliku... Zrestartuj program i sprobuj ponownie.\n\n";
        v.push_back(0);
        return v;
    }
}

/* Nie wiedziałem jak ominąć zwracanie wektora, jeśli plik się nie otworzył, więc zwracam jednoelementowy wektor z zerem. */

void zadanie_4() {

    std::cout << "\n\nZadanie 4.\n\n";

    std::string temp_dir = "";

    std::cout << "Podaj sciezke do pliku tekstowego, z ktorego chcesz wczytac wektor liczb calkowitych:\n\n";
    std::cin.ignore();
    std::getline(std::cin, temp_dir);

    std::cout << "\n\nOto wczytany wektor:\n\n";
    printIntVector_vertical(readIntVector(temp_dir));

    AbyKontynuowac();
}



/* Zadanie 5. */

void swap(int& a, int& b) {

    int temp = a;
    a = b;
    b = temp;
}

void bubbleSort (int_Vector& v) {

    for (unsigned int n = v.size(); n > 1; --n) {

        for (unsigned int i = 0; i < n; ++i) {

            if ( v[i] > v[i+1] ) {
                swap( v[i], v[i+1] );
            }
        }
    }
}

/*
    Z racji, że funkcja "bubbleSort" ma bezpośrednio posortować (zmodyfikować) istniejący poza nią wektor, należy
    przekazać go do niej przez referencję. Oszczędza to też pamięć, bo nie jest tworzona kopia wektora, jak w przypadku
    przekazywania przez wartość.
*/

//          "Cholera, to nawet działa..." - zdziwił się programista i poszedł po podwyżkę.

void zadanie_5() {

    std::cout << "\n\nZadanie 5.\n\n"
                 "Jak dlugi wektor posortowac?\n\n";
    unsigned int n = 0;
    std::cin.ignore();
    std::cin >> n;
    int_Vector v1 = randomIntVector(n, -999, 999);

    bubbleSort(v1);

    std::string temp_dir = "";
    std::cout << "\n\nPodaj sciezke do pliku tekstowego, w ktorym zostanie zapisany posortowany wektor:\n\n";
    std::cin.ignore();
    std::getline(std::cin, temp_dir);
    writeVector(v1, temp_dir);

    AbyKontynuowac();
}



/* Zadanie 6. */

int_Matrix createMatrix (const std::array <unsigned int, 2>& shape) {

    int_Matrix Matrix;

    Matrix.resize(shape[0]);

    for (int i=0; i < shape[0]; ++i) {

        Matrix[i].resize(shape[1], 0);
    }

    return Matrix;
}

int_Matrix randomMatrix (const std::array <unsigned int, 2>& shape, const int& min, const int& max) {

    int_Matrix Matrix;


    Matrix.resize(shape[0]);


    for (int i=0; i < shape[0]; ++i) {

        Matrix[i].resize(shape[1]);

        for (int j=0; j < shape[1]; ++j) {

            Matrix[i][j] = randomInt(min, max);
        }
    }

    return Matrix;
}

void printMatrix (const int_Matrix& Matrix) {

    std::cout << "\n";

    for (int i=0; i < Matrix.size(); ++i) {

        std::cout << "\n\n\t";

        for (int j=0; j+1 < Matrix[i].size(); ++j) {

            std::cout << Matrix[i][j] << "\t";
        }

        std::cout << Matrix[i][ Matrix[i].size() - 1 ];
    }

    std::cout << "\n\n\n";
}

/*
    W funkcji "printMatrix" można przekazać macierz przez stałą referencję, ponieważ jej elementy
    są jedynie odczytywane, a nie w jakikolwiek sposób zmieniane.
*/

void zadanie_6() {

    std::cout << "\n\nZadanie 6.\n\n";

    unsigned int m = 5, n = 4;
    int_Matrix A = createMatrix({m, n});
    printMatrix(A);

    int_Matrix B = randomMatrix({m, n}, -5, 5);
    printMatrix(B);

    AbyKontynuowac();
}



/* Zadanie 7. */

int_Matrix add_2Matrices (int_Matrix A, const int_Matrix& B) {

    for (unsigned int i=0; i < A.size(); ++i) {

        for (unsigned int j=0; j < A[i].size(); ++j) {

            A[i][j] += B[i][j];
        }
    }

    return A;
}

int_Matrix subtract_2Matrices (int_Matrix A, const int_Matrix& B) {

    for (unsigned int i=0; i < A.size(); ++i) {

        for (unsigned int j=0; j < A[i].size(); ++j) {

            A[i][j] -= B[i][j];
        }
    }

    return A;
}

void zadanie_7() {

    std::cout << "\n\nZadanie 7.\n\n"
                 "Dodawanie i odejmowanie macierzy:\n\n"
                 "Podaj liczbe wierszy:\n\n";
    unsigned int m=0, n=0;
    std::cin.ignore();
    std::cin >> m;

    std::cout << "\n\nPodaj liczbe kolumn:\n\n";
    std::cin.ignore();
    std::cin >> n;

    int_Matrix A = randomMatrix({m, n}, -16, -4);
    int_Matrix B = randomMatrix({m, n}, -16, -4);

    std::cout << "\n\nPierwsza macierz:";
    printMatrix(A);
    std::cout << "Druga macierz:";
    printMatrix(B);

    std::cout << "Suma macierzy:";
    printMatrix(add_2Matrices(A, B));

    std::cout << "Roznica macierzy:";
    printMatrix(subtract_2Matrices(A, B));

    AbyKontynuowac();
}



/* Zadanie 8. */

int_Matrix multiply_2Matrices (const int_Matrix& A, const int_Matrix& B) {

    int_Matrix C = createMatrix({ A.size(), B[0].size() });

    for (unsigned int i=0; i < C.size(); ++i) {

        for (unsigned int j=0; j < C[i].size(); ++j) {

            for (unsigned int k=0; k < A[i].size(); ++k) {

                C[i][j] += A[i][k]*B[k][j];
            }
        }
    }

    return C;
}

void zadanie_8() {

    unsigned int m=0, n=0, p=0;

    std::cout << "\n\nZadanie 8.\n\n"
                 "Mnozenie macierzy:\n\n"
                 "Podaj liczbe kolumn pierwszej macierzy, bedaca jednoczesnie liczba wierszy drugiej macierzy:\n\n";
    std::cin.ignore();
    std::cin >> p;

    std::cout << "\n\nPodaj liczbe wierszy pierwszej macierzy:\n\n";
    std::cin.ignore();
    std::cin >> m;

    std::cout << "\n\nPodaj liczbe kolumn drugiej macierzy:\n\n";
    std::cin.ignore();
    std::cin >> n;

    int_Matrix A = randomMatrix({m, p}, 1, 14);
    int_Matrix B = randomMatrix({p, n}, 1, 14);

    std::cout << "\n\nPierwsza macierz:";
    printMatrix(A);
    std::cout << "Druga macierz:";
    printMatrix(B);

    std::cout << "Iloczyn macierzy:";
    printMatrix(multiply_2Matrices(A, B));

    AbyKontynuowac();
}



/* Zadanie 9. */

double inv (double x) {

    return 1/x;
}

double inv_squared (double x) {

    return 1/(x*x);
}

void forEach (float_Matrix& m, double (&func)(double)) {

    for (int i=0; i < m.size(); ++i) {

        for (int j=0; j < m[i].size(); ++j) {

            m[i][j] = func(m[i][j]);
        }
    }
}

/*
    I tego już totalnie nie rozumiem. W sensie, tego na jakiej zasadzie działa przekazywanie funkcji jako argumentu
    dla innej funkcji. Nie było żadnego wyjaśnienia w PDFie, a jak szukałem na necie, to znalazłem tylko jakieś
    skomplikowane rzeczy o funktorach, ale to już był poziom klas, czyli póki co mnie przerastający.

    Było jeszcze coś fajnego i zrozumiałego o poleceniu
    "std::function<zwracany_typ(typ_argumentu_nr_1, typ_argumentu_nr_2 [...])> nazwa", ale widziałem,
    że te czary, które Pan odstawił w przykładzie implementacji "forEach" w zad. 9 nie opierały się akurat na tym :C

    Domyślam się jedynie, że pierwszy człon to zwracany typ, w pierwszym nawiasie jest robocza nazwa funkcji
    (używana jedynie w ramach deklaracji funkcji nadrzędnej), a w drugim typ argumentu nr 1 (w tym przypadku jedynego)
    przyjmowanego przez funkcję podrzędną.

    Nie wiem jedynie, po co jest ten ampersent przed nazwą funkcji podrzędnej.
*/

void printFloatMatrix (const float_Matrix& Matrix) {

    std::cout << "\n";

    for (unsigned int i=0; i < Matrix.size(); ++i) {

        std::cout << "\n\n\t";

        for (unsigned int j=0; j+1 < Matrix[i].size(); ++j) {

            std::cout << Matrix[i][j] << "\t";
        }

        std::cout << Matrix[i][ Matrix[i].size() - 1 ];
    }

    std::cout << "\n\n\n";
}

void zadanie_9() {

    std::cout << "\n\nZadanie 9.\n\n";

    float_Matrix m1 {{4.56, 3.45, 2.34, 0.73}, {7.89, 1.68, 2.34, 5.89}, {1.12, 0.99, 3.18, 4.20}, {5.67, 2.87, 9.37, 21.37}};

/*
    Nie wiedziałem jak wygenerować zmiennoprzecinkowe liczby pseudolosowe xD
*/

    printFloatMatrix(m1);

    std::cout.precision(4);

    forEach(m1, sqrt);
    printFloatMatrix(m1);

    forEach(m1, inv);
    printFloatMatrix(m1);

    forEach(m1, inv_squared);
    printFloatMatrix(m1);

    std::cout.precision(6);

    AbyKontynuowac();
}



int main() {

    bool stop_menu = false;
    while (!stop_menu) {

        char wybor = 'a';

        std::cout << "\n\nWitaj w przegladzie zadan z laboratorium 5.! Lista opcji:\n\n"
                "\t\"2\" - Zadanie 2. (porownywanie, dodawanie i odejmowanie wektorow oraz mnozenie wektora przez skalar)\n"
                "\t\"3\" - Zadanie 3. (zapisywanie wektora do pliku)\n"
                "\t\"4\" - Zadanie 4. (wczytywanie wektora z pliku)\n"
                "\t\"5\" - Zadanie 5. (sortowanie wektora Bubble Sortem)\n"
                "\t\"6\" - Zadanie 6. (test funkcji do obslugi macierzy, ale z macierzami przekazywanymi przez referencje)\n"
                "\t\"7\" - Zadanie 7. (dodawanie i odejmowanie macierzy)\n"
                "\t\"8\" - Zadanie 8. (mnozenie macierzy)\n"
                "\t\"9\" - Zadanie 9. (funkcja \"forEach\", wywolujaca dana funkcje dla kazdego elementu macierzy)\n"
                "\t\";\" (srednik) - Wyjdz z programu.\n\n\t";

        std::cin >> wybor;
        switch (wybor) {
            case '2': {zadanie_2(); break;}
            case '3': {zadanie_3(); break;}
            case '4': {zadanie_4(); break;}
            case '5': {zadanie_5(); break;}
            case '6': {zadanie_6(); break;}
            case '7': {zadanie_7(); break;}
            case '8': {zadanie_8(); break;}
            case '9': {zadanie_9(); break;}
            case ';': {stop_menu = true; break;}
            default: {std::cout << "\n\nNie wybrano zadnej opcji. Restartuje..."; break;}
        }
    }

    return 0;
}