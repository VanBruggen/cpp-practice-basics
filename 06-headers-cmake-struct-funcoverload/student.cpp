#include "student.hpp"



//======================================================================================================================
void displayStudent (const Student& s) {

    std::cout << "\n\nOto informacje o podanej studentce / studencie:\n\n"
              << "Imie:\t\t"            << s.imie << "\n"
              << "Nazwisko:\t"          << s.nazwisko << "\n"
              << "Data urodzenia:\t"    << s.dataUr.dzien << "-" << s.dataUr.mies << "-" << s.dataUr.rok << "\n"
              << "Nr indeksu:\t"        << s.nrIndeksu << "\n\n\n";
}

//======================================================================================================================