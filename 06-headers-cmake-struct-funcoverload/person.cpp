#include "person.hpp"



//======================================================================================================================
Person createPerson() {

    Person p;

    std::cout << "\n\nPodaj dane nowej osoby:\n"
                 "Imie:\n";
    std::cin.ignore();
    std::getline (std::cin, p.firstName);

    std::cout << "\nNazwisko:\n";
    std::getline (std::cin, p.lastName);

    std::cout << "\nWiek:\n";
    std::cin >> p.age;

    std::cout << "\nPlec (K/M):\n";
    std::cin >> p.gender;

    return p;
}

//======================================================================================================================
void displayPerson (const Person& p) {

    std::cout <<    "\n\nImie:\t\t"    << p.firstName
              <<    "\nNazwisko:\t"    << p.lastName
              <<    "\nWiek:\t\t"      << p.age
              <<    "\nPlec:\t\t"      << p.gender
              <<    "\n\n";
}

//======================================================================================================================
PersonList createPersonList (const unsigned int& n) {

    PersonList L1;
    L1.resize(n);

    for (unsigned int i=0; i < n; ++i) {

        L1[i] = createPerson();
    }

    return L1;
}

//======================================================================================================================
std::fstream plik;

void writePersonList (const PersonList& L) {

    plik.open("D:/zadania_na_PP/Laboratorium_6/personae.txt", std::ios::out | std::ios::trunc);

    if ( plik.is_open() && plik.good() ) {

        for (unsigned int i=0; i+1 < L.size(); ++i) {

            plik << L[i].firstName << ' ' << L[i].lastName << '\t' << L[i].age << '\t' << L[i].gender << '\n';
        }

        plik << L[L.size() - 1].firstName << ' ' << L[L.size() - 1].lastName << '\t'
              << L[L.size() - 1].age << '\t' << L[L.size() - 1].gender;

        plik.close();
        std::cout << "\n\nOtworz plik w notatniku i sprawdz, czy wszystko gra i trabi :)";

    } else {

        plik.close();
        std::cout << "\n\nNie udalo sie otworzyc pliku... Zrestartuj program i sprobuj ponownie.";
    }
}

//======================================================================================================================
PersonList readPersonList () {

    PersonList L(0);

    plik.open ("D:/zadania_na_PP/Laboratorium_6/personae.txt", std::ios::in);

    if ( plik.is_open() && plik.good() ) {

        Person temp;

        while (!plik.eof()) {

            plik >> temp.firstName;

            if (plik.eof()) {break;}

            plik >> temp.lastName;
            plik >> temp.age;
            plik >> temp.gender;

            L.push_back(temp);
        }

        plik.close();
        return L;

    } else {

        plik.close();
        std::cout << "\n\nNie udalo sie otworzyc pliku... Zrestartuj program i sprobuj ponownie.";
        return L;
    }
}

//======================================================================================================================
void displayPersonList (const PersonList& L) {

    for (unsigned int i=0; i < L.size(); ++i) {

        displayPerson(L[i]);
    }
}

//======================================================================================================================
float averageAgeOnPersonList (const PersonList& L) {

    if (L.size() > 0) {

        unsigned short suma = 0;

        for (unsigned short i = 0; i < L.size(); ++i) {

            suma += L[i].age;
        }

        return (float(suma) / L.size() );

    } else {

        return 0;
    }
}

//======================================================================================================================
void swap_2Personae(Person& a, Person& b) {

    Person temp = a;
    a = b;
    b = temp;
}

void bubbleSortByAge (PersonList& L) {

    for (unsigned short n = L.size(); n > 1; --n) {

        for (unsigned short i = 0; i < n; ++i) {

            if ( L[i].age > L[i+1].age ) {

                swap_2Personae( L[i], L[i+1] );
            }
        }
    }
}

//======================================================================================================================