#ifndef LABORATORIUM_6_STUDENTY_HPP
#define LABORATORIUM_6_STUDENTY_HPP

#include <iostream>



//======================================================================================================================
struct T_dataUr {

    unsigned short dzien;
    unsigned short mies;
    unsigned short rok;
};

//======================================================================================================================
struct Student {

    std::string imie;
    std::string nazwisko;
    T_dataUr dataUr;
    unsigned int nrIndeksu;
};

//======================================================================================================================
void displayStudent (const Student& s);

//======================================================================================================================



#endif /* LABORATORIUM_6_STUDENTY_HPP */
