#include "person.hpp"
#include "student.hpp"



void AbyKontynuowac() {

    std::cout << "\n\n\nAby kontynuowac, wprowadz \";\" (srednik).\n\n";

    char c = 'a';

    do {
        c = getchar();
    } while (c != ';');

    std::cout << "\n\n";
}



/* Zadanie 1. */

void zadanie_1() {

    std::cout <<    "\n\nZadanie 1.\n\n"
                    "Oto glowni bohaterowie filmu \"Wsciekle Piesci Weza\":";

    Person p1, p2;

    p1.firstName    = "Mateusz";
    p1.lastName     = "Molibdenowy";
    p1.age          = 44;
    p1.gender       = 'M';

    p2.firstName    = "Janusz";
    p2.lastName     = "Tytanowy";
    p2.age          = 60;
    p2.gender       = 'M';

    std::cout <<    "\n\nImie:\t\t"      << p1.firstName
              <<    "\nNazwisko:\t"    << p1.lastName
              <<    "\nWiek:\t\t"        << p1.age
              <<    "\nPlec:\t\t"        << p1.gender
              <<    "\n\n";

    std::cout <<    "\n\nImie:\t\t"      << p2.firstName
              <<    "\nNazwisko:\t"    << p2.lastName
              <<    "\nWiek:\t\t"        << p2.age
              <<    "\nPlec:\t\t"        << p2.gender
              <<    "\n\n";

    AbyKontynuowac();
}



/* Zadanie 2. */

void zadanie_2() {

    std::cout << "\n\nZadanie 2.\n\n";

    Person p1 = createPerson();
    Person p2 = createPerson();

    displayPerson(p1);
    displayPerson(p2);

    AbyKontynuowac();
}



/* Zadanie 3. */

void zadanie_3() {

    std::cout << "\n\nZadanie 3.\n\n";

    Student s1;
    s1.imie = "Janusz";
    s1.nazwisko = "Tytanowy Junior";
    s1.dataUr.dzien = 25; s1.dataUr.mies = 2; s1.dataUr.rok = 1997;
    s1.nrIndeksu = 132137;

    displayStudent(s1);

    AbyKontynuowac();
}

/*
    Akurat ja zawsze zapobiegam takim problemom, poprzez odpowiednio precyzyjne nazywanie funkcji, ale gdyby obydwie
    nazywały się tak samo, to kompilator rozpoznałby, którą ma wybrać po typie przekazywanego do niej parametru.
*/



/* Zadanie 4. */

void zadanie_4() {

    unsigned int n=0;

    std::cout << "\n\nZadanie 4.\n\n"
                 "Podaj zadana liczbe osob na liscie:\n";
    std::cin.ignore();
    std::cin >> n;

    PersonList L = createPersonList(n);

    writePersonList(L);

    AbyKontynuowac();
}



/* Zadanie 5. */

void zadanie_5() {

    PersonList L = readPersonList();

    std::cout << "\n\nZadanie 5.\n\n"
                 "Zawartosc wczytanej listy:";

    displayPersonList(L);

    std::cout << "\nSrednia wieku osob z wczytanej listy:\t" << averageAgeOnPersonList(L);

    AbyKontynuowac();
}



/* Zadanie 6. */

void zadanie_6() {

    std::cout << "\n\nZadanie 6.\n\n";

    PersonList L = readPersonList();

    bubbleSortByAge(L);

    displayPersonList(L);
}

/*
    Nie, w ogóle nie działa. Wyrzuca jedynie błąd "std::bad_alloc", że niby za mało pamięci albo wyciek pamięci.
    Nie wiem o co chodzi. Szukałem też w necie, ale nie znalazłem niczego, co pasowałoby do mojego przypadku.
*/



int main() {

    bool stop_menu = false;
    while (!stop_menu) {

        char wybor = 'a';

        std::cout << "\n\nWitaj w przegladzie zadan z laboratorium 6.! Lista opcji:\n\n"
                "\t\"1\" - Zadanie 1.\n"
                "\t\"2\" - Zadanie 2.\n"
                "\t\"3\" - Zadanie 3.\n"
                "\t\"4\" - Zadanie 4.\n"
                "\t\"5\" - Zadanie 5.\n"
                "\t\"6\" - Zadanie 6.\n"
                "\t\";\" (srednik) - Wyjdz z programu.\n\n\t";

        std::cin >> wybor;
        switch (wybor) {
            case '1': {zadanie_1(); break;}
            case '2': {zadanie_2(); break;}
            case '3': {zadanie_3(); break;}
            case '4': {zadanie_4(); break;}
            case '5': {zadanie_5(); break;}
            case '6': {zadanie_6(); break;}
            case ';': {stop_menu = true; break;}
            default: {std::cout << "\n\nNie wybrano zadnej opcji. Restartuje..."; break;}
        }
    }

    return 0;
}