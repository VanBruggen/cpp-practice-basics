#include <iostream>
#include <conio.h>
#include <cmath>



void AbyKontynuowac() {
    std::cout << "\n\nAby kontynuowac, wcisnij \"ENTER\".\n\n";

    getch();
}



/* Zadanie 1. */

// Hehe, zrobiłem niechcący na poprzednim laboratorium xD



/* Zadanie 2. */

// Tak samo, jak z zadaniem 1., z tym że tutaj wypada mi przynajmniej napisać jakiś komentarz:
// Generalnie wystarczy zrzutować tylko jedną liczbę calkowitą na zmiennoprzecinkową, żeby wynik też był zmiennoprzecinkowy.
// Wyczytałem to już wcześniej na jakimś forum XD



/* Zadanie 3. */

// Linie kodu analizujące rozmiar hipotetycznych typów pochodnych, ale niedziałające, ze względu na brak takich typów, zaznaczone są komentarzami

void zadanie_3() {
    std::cout << "\n\nZadanie 3.\n\n";

    std::cout << "Rozmiar pamieci (w bajtach) uzywanej przez podstawowe typy danych:"

            "\n\nint = " << sizeof(int) <<
            "\nchar = " << sizeof(char) <<
            "\nbool = " << sizeof(bool) <<
            "\nshort = " << sizeof(short) <<
            "\nlong = " << sizeof(long) <<
            "\nfloat = " << sizeof(float) <<
            "\ndouble = " << sizeof(double) <<

            "\n\nRozmiar pamieci uzywanej przez derywaty podstawowych typow:" <<

            "\n\nshort int = " << sizeof(short int) <<
            "\nlong int = " << sizeof(long int) <<
            "\nsigned int = " << sizeof(signed int) <<
            "\nunsigned int = " << sizeof(unsigned int) <<

            // "\n\nshort char = " << sizeof(short char) <<
            // "\nlong char = " << sizeof(long char) <<
            "\n\nsigned char = " << sizeof(signed char) <<
            "\nunsigned char = " << sizeof(unsigned char) <<

            // "\n\nshort bool = " << sizeof(short bool) <<
            // "\nlong bool = " << sizeof(long bool) <<
            // "\nsigned bool = " << sizeof(signed bool) <<
            // "\nunsigned bool = " << sizeof(unsigned bool) <<

            // "\n\nshort short = " << sizeof(short short) <<
            // "\nlong short = " << sizeof(long short) <<
            "\n\nsigned short = " << sizeof(signed short) <<
            "\nunsigned short = " << sizeof(unsigned short) <<

            // "\n\nshort long = " << sizeof(short long) <<
            "\n\nlong long = " << sizeof(long long) <<
            "\nsigned long = " << sizeof(signed long) <<
            "\nunsigned long = " << sizeof(unsigned long) <<

            // "\n\nshort float = " << sizeof(short float) <<
            // "\nlong float = " << sizeof(long float) <<
            // "\nsigned float = " << sizeof(signed float) <<
            // "\nunsigned float = " << sizeof(unsigned float) <<

            // "\n\nshort double = " << sizeof(short double) <<
            "\n\nlong double = " << sizeof(long double) << "\n\n";
            // "\nsigned double = " << sizeof(signed double) <<
            // "\nunsigned double = " << sizeof(unsigned double) <<
}



/* Zadanie 4. */

void zadanie_4() {
    std::cout << "\n\nZadanie 4.\n\n";

    std::cout
              << "\n\nWitaj w wyswietlaczu liczby zmiennoprzecinkowej z rozna dokladnoscia i w roznym trybie!\n"
              "Podaj liczbe, ktora chcesz wyswietlic:\n\n";

    float liczba = 0;
    std::cin >> liczba;

    std::cout
              << "\n\nDomyslna dokladnosc (stala): " << std::fixed << liczba << "\n"
              "Notacja naukowa: " << std::scientific << liczba << "\n"
              "System szesnastkowy (heksadecymalny): " << std::hexfloat << liczba << "\n"
              << std::fixed << "Dokladnosc do 3 miejsc po przecinku: ";
    std::cout.precision(3);
    std::cout
              << liczba << "\n"
              "Notacja naukowa z dokladnoscia do 3 miejsc po przecinku: " << std::scientific << liczba << "\n\n";

    std::cout.precision(6);
}

    /*
       Odkryłem, że polecenie "std::cout.precision(x)" ustawia dokładność couta już na stałe i globalnie, więc nie było
       potrzeby robić tego ponownie dla notacji naukowej. Trzeba tylko na końcu bloku ustawić dokładność z powrotem na
       domyślną.

       Poza tym, żeby wrócić z trybu heksadecymalnego do decymalnego, należy użyć polecenia "std::fixed" po
       daszkach strumieniowych.
    */



/* Zadanie 5. */

void zadanie_5() {
    std::cout << "\n\nZadanie 5.";

    float liczba1=0, liczba2=0;

    std::cout << "\n\nWitaj w prostym kalkulatorze! Podczas ciagu dzialan, dostepny bedzie prymitywny wyswietlacz,\n"
                 "pokazujacy chwilowy wynik. Kalkulator moze wykonywac obliczenia na kolejnych liczbach\n"
                 "zmiennoprzecinkowych (UWAGA! Wpisujac liczby, zamiast przecinka uzywaj kropki!),\n"
                 "dopoki nie zakonczysz jego pracy albo go nie zresetujesz.\n\n\t"
                 "Jesli chcesz rozpoczac prace kalkulatora, wpisz \"1\".\n\n\t"
                 "Jesli chcesz wyjsc z kalkulatora, wpisz \";\" (srednik):\n\n\t";

    bool stop_01 = false;

    bool stop_00 = false;
    while (!stop_00) {

        char znak = 'a';

        std::cin >> znak;

        if (znak == '1') {
            stop_00 = true;
            std::cout << "\n\n\tPodaj pierwsza liczbe:\n\n\t";
            std::cin >> liczba1;
        }

        if (znak == ';') {
            stop_00 = true;
            stop_01 = true;
        }

        else {
            std::cout << "\n\n\tNieprawidlowy znak sterujacy.\n\n\t"
                         "Jesli chcesz rozpoczac prace kalkulatora, wpisz \"1\".\n\n\t"
                         "Jesli chcesz wyjsc z kalkulatora, wpisz \";\" (srednik):\n\n\t";
        }
    }

    while (!stop_01) {

        std::cout << "\n\nJakie dzialanie chcesz teraz wykonac? Kolejna liczbe mozesz podac dopiero po tym etapie.\n\n"
                     "Lista dostepnych dzialan:\n\n"
                     "\t\"+\" - Dodawanie.\n"
                     "\t\"-\" - Odejmowanie.\n"
                     "\t\"*\" - Mnozenie.\n"
                     "\t\"/\" - Dzielenie.\n"
                     "\t\"c\" - Reset.\n"
                     "\t\";\" - Wyjscie z kalkulatora.\n\n"
                     "\tWyswietlacz: " << liczba1 << "\n\n"
                     "\tAby wybrac konkretna opcje, wprowadz jej znak:\n\n\t";
        char znak_dzialania = 'a';

        bool stop_02 = false;
        while (!stop_02) {

            std::cin >> znak_dzialania;

            if (znak_dzialania == ';') {

                std::cout << "\n\nWyswietlacz (ostatni stan przed wylaczeniem): " << liczba1;

                stop_01 = true;
                stop_02 = true;
                continue;
            }

            if (znak_dzialania == 'c') {
                std::cout << "\n\nWyswietlacz (ostatni stan przed resetem): " << liczba1
                          << "\n\nResetuje...\n\n\tPodaj pierwsza liczbe:\n\n\t";
                std::cin >> liczba1;
                stop_02 = true;
                continue;
            }

            if (znak_dzialania != '+' && znak_dzialania != '-' && znak_dzialania != '*' && znak_dzialania != '/') {
                std::cout << "\n\nTy no... Ale podaj obslugiwany znak, a nie jakis nieprzewidziany. Jeszcze raz.\n"
                             "Przypomne Ci tylko opcje:\n\n"
                             "\t\"+\" - Dodawanie.\n"
                             "\t\"-\" - Odejmowanie.\n"
                             "\t\"*\" - Mnozenie.\n"
                             "\t\"/\" - Dzielenie.\n"
                             "\t\"c\" - Reset.\n"
                             "\t\";\" - Wyjscie z kalkulatora.\n\n"
                             "\tWyswietlacz: " << liczba1 << "\n\n\t";
            } else { stop_02 = true; }
        }

        if (znak_dzialania == ';' || znak_dzialania == 'c') {
            continue;
        }

        std::cout << "\n\n\tTeraz podaj kolejna liczbe:\n\n\t";

        bool stop_03 = false;
        while (!stop_03) {

            std::cin >> liczba2;

            if (liczba2 == 0 && znak_dzialania == '/') {
                std::cout << "\n\n\tNie dziel przez zero, cholero! Jeszcze raz podaj kolejna liczbe:\n\n\t";
            } else { stop_03 = true; }
        }

        switch (znak_dzialania) {
            case '+':
                liczba1 += liczba2;
                break;
            case '-':
                liczba1 -= liczba2;
                break;
            case '*':
                liczba1 *= liczba2;
                break;
            case '/':
                liczba1 /= liczba2;
                break;
        }
    }

    std::cout << "\n\nKalkulator zakonczyl prace.";

    AbyKontynuowac();
}

/*
  Aby zapauzować program:
  1) <conio.h> + getch()  ---> Zdało egzamin (@Edit: Jednak nie do końca, bo nie zawsze działa, zwłaszcza, jak się powtórzy blisko siebie)
  2) <cstdio> + getchar() ---> Nie testowałem
  3) std::cin.get()       ---> Nie zdało egzaminu
  4)                      ---> Nie zdało egzaminu

     #include <limits>

     void AbyKontynuowac_v1() {
     std::cout << "\n\nAby kontynuowac, wcisnij \"ENTER\".\n\n";
     std::cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
     }

  5)                      ---> Nie testowałem

     #include <cstdio>

     void AbyKontynuowac_v2() {
     char c = 'a';
     std::cout << "\n\nAby kontynuowac, wcisnij \"ENTER\".\n\n";
     do {
        c = getchar();
        } while (c != '\n' && c != EOF);
     }
*/



/* Zadanie 6. */

void zadanie_6() {
    std::cout << "\n\nZadanie 6.\n\n";

    std::cout << "Witaj w procedurze testowej pre- i post-inkrementacji (oraz dekrementacji)!\n"
                 "Podaj liczbe calkowita:\n\n";
    int liczba = 0;
    std::cin >> liczba;
    std::cout << "\n\nPreinkrementacja: " << liczba << ", ";
    std::cout <<  ++liczba << ", ";
    std::cout <<  liczba;
    std::cout << "\nPostinkrementacja: " << liczba << ", ";
    std::cout <<  liczba++ << ", ";
    std::cout <<  liczba;
    std::cout << "\nPredekrementacja: " << liczba << ", ";
    std::cout <<  --liczba << ", ";
    std::cout <<  liczba;
    std::cout << "\nPostdekrementacja: " << liczba << ", ";
    std::cout <<  liczba-- << ", ";
    std::cout <<  liczba;

    AbyKontynuowac();
}



/* Zadanie 7. */

void zadanie_7() {
    std::cout << "\n\nZadanie 7.\n\n";

    std::cout << "Witaj w wypisywaczu liczb od 1 do 100, uzywajac trzech roznych typow petli!";

    AbyKontynuowac();

    std::cout << "\n\n\"for\":\n\n";
    for (int i = 1; i<=100; ++i) {
        std::cout << i << "\n";
    }

    std::cout << "\n\n\"while\":\n\n";
    int k = 1;
    while (k <= 100) {
        std::cout << k << "\n";
        ++k;
    }

    std::cout << "\n\n\"do-while\":\n\n";
    int j = 1;
    do {
        std::cout << j << "\n";
        ++j;
    } while (j<=100);

    AbyKontynuowac();
}



/* Zadanie 8. */

void zadanie_8() {
    std::cout << "\n\nZadanie 8.\n\n";

    std::cout << "Oto wszystkie podzielne przez 7 liczby z zakresu od 1 do 500:\n\n";

    AbyKontynuowac();

    for (int i = 1; i <= 500; ++i) {
        if (i % 7 == 0) {
            std::cout << i << "\n";
        }
    }

    AbyKontynuowac();
}



/* Zadanie 9. */

void zadanie_9() {
    std::cout << "\n\nZadanie 9.\n\n";

    std::cout << "Oto suma odwrotnosci kwadratow liczb od 1 do 10.000 i od 10.000 do 1:\n\n";

    float suma = 0;

    std::cout << "Kolejnosc - od poczatku:\n\n";
    for (int i = 1; i <= 10000; ++i) {
        suma += 1.0/(i*i);                              // Zamiast rzutować jedynkę na float, można po prostu wpisać ją z kropką i zerem
    }

    std::cout << suma << "\n\n";

    AbyKontynuowac();

    suma = 0;

    std::cout << "Kolejnosc - od konca:\n\n";
    for (int k = 10000; k >= 1; --k) {
        suma += 1.0/(k*k);
    }

    std::cout << suma;

    AbyKontynuowac();
}

  /*
  Sumy różnią się w niewielkim stopniu, ale jednak nie są identyczne. Podejrzewam, że ma to związek z dokładnością sumy, spadającą
  z każdą  kolejną iteracją.
  */



/* Zadanie 10. */

void zadanie_10() {
    std::cout << "\n\nZadanie 10.\n\n";

    float wynik = 0;

    for (int n = 0; n <= 100; ++n) {
        if (n == 0) {
            wynik = 1;
        }

        if (n % 2 == 0 && n > 0) {
            wynik += 1.0/(2*n + 1);
        }

        if (n % 2 == 1) {
            wynik += (-1.0)/(2*n + 1);
        }
    }

    /*

    Alternatywnie:

    for (int n = 0; n <= 20000; ++n) {
        wynik += pow(-1, n)/(2*n + 1);
    }

    Wydaje mi się jednak, że ten wariant ma większą złożoność obliczeniową, dlatego zaimplementowałem swój,
    z porównaniami zamiast potęgowania.

    */

    std::cout << "Oto wynik pomnozony przez 4:\n\n";

    std::cout << wynik * 4;

    AbyKontynuowac();
}

/* Spoiler: Wyszła liczba Pi. */



int main() {

    bool stop_menu = false;
    while (!stop_menu) {

        char wybor = 'a';

        std::cout << "\n\nWitaj w przegladzie zadan z laboratorium nr 2! Lista opcji:\n\n"
                     "\t\"1\" - Zadanie 1.\n"
                     "\t\"2\" - Zadanie 2.\n"
                     "\t\"3\" - Zadanie 3.\n"
                     "\t\"4\" - Zadanie 4.\n"
                     "\t\"5\" - Zadanie 5.\n"
                     "\t\"6\" - Zadanie 6.\n"
                     "\t\"7\" - Zadanie 7.\n"
                     "\t\"8\" - Zadanie 8.\n"
                     "\t\"9\" - Zadanie 9.\n"
                     "\t\"0\" (zero!) - Zadanie 10.\n"
                     "\t\";\" (srednik) - Wyjdz z programu.\n\n\t";

        std::cin >> wybor;
        switch (wybor) {
            case '1': {std::cout << "\n\nHehe, zrobione na poprzednim laboratorium XD Ale zamiescilem do niego komentarz w kodzie."; break;}
            case '2': {std::cout << "\n\nHehe, zrobione na poprzednim laboratorium XD Ale zamiescilem do niego komentarz w kodzie."; break;}
            case '3': {zadanie_3(); break;}
            case '4': {zadanie_4(); break;}
            case '5': {zadanie_5(); break;}
            case '6': {zadanie_6(); break;}
            case '7': {zadanie_7(); break;}
            case '8': {zadanie_8(); break;}
            case '9': {zadanie_9(); break;}
            case '0': {zadanie_10(); break;}
            case ';': {stop_menu = true; break;}
            default: {std::cout << "\n\nNie wybrano zadnej opcji. Restartuje..."; break;}
        }
    }

    return 0;
}