# cpp-practice-basics

Language: Polish.

Each consecutive exercise is contained within its own folder, the name of which consists of every new subject touched upon in a given exercise.

I have deliberately not added CMake files to `.gitignore`.