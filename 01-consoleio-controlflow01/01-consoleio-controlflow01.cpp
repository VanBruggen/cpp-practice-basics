#include <iostream>                   /* Nawet nie wiedziałem, że już nie potrzeba biblioteki <string> :D */

/* Zadanie 1. */

void zadanie_1() {
    std::cout << "\n\nZadanie 1.\n\n";

    std::string imie, nazwisko;
    imie = "";
    nazwisko = "";

    int wiek;
    wiek = 0;

    std::cout <<"\n\nWitaj!\n\tJak Ci na imie?\n\n\t";
    std::cin >>imie;
    std::cout <<"\n\n\tA jak sie nazywasz?\n\n\t";
    std::cin >>nazwisko;
    std::cout <<"\n\n\tIle masz lat?\n\n\t";

    bool stop_wiek = false;
    while (!stop_wiek) {
        std::cin >>wiek;
        if (wiek < 0) {std::cout << "\n\nUjemna liczba wiosen? Oj Ty smieszku... Wprowadz wiek jeszcze raz:\n\n";}
        else {stop_wiek = true;}
    }

    std::cout <<"\n\n\tPodsumowujac: jestes "<<imie<<" "<<nazwisko<<", lat: "<<wiek<<". Bez odbioru.\n\n";
}

/* Zadanie 2. */

void zadanie_2() {
    std::cout << "\n\nZadanie 2.\n\n";
    int temp1=0,temp2=0;
    std::cout << "\n\nWitaj w prymitywnym kalkulatorze Krognaka Barbarzyncy!\n\n"
            "Wprowadz dwie liczby calkowite, a wnet ujrzysz wyniki czterech podstawowych dzialan na nich.\n"
            "Kolejnosc obowiazuje. Zacznijmy od pierwszej liczby:\n\n";
    std::cin>>temp1;
    std::cout<<"\n\nDziekowac. Teraz druga:\n\n";
    bool stop = false;
    while (!stop) {
        std::cin >> temp2;
        if (temp2 == 0) {std::cout << "\n\nAle nie zero! Nie pamietasz, co w podstawowce mowila pani na matmie?"
                    " Nie dziel przez zero, cholero! Jeszcze raz:\n\n";}
        else {stop = true;}
    }
    std::cout<<"Now the magic happens...\n\n"
             << temp1 << "+" << temp2 << "=" << temp1 + temp2 <<"\n"
             << temp1 << "-" << temp2 << "=" << temp1 - temp2 <<"\n"
             << temp1 << "*" << temp2 << "=" << temp1 * temp2 <<"\n"
             << temp1 << "/" << temp2 << "=" << float(temp1) / temp2 <<"\n"
             << "\n\t\tKrognak i ja dziekujemy za uwage. Bywaj w zdrowiu, wedrowcze!\n\n";
}

/* Zadanie 3. */

void zadanie_3() {
    std::cout << "\n\nZadanie 3.\n\n";
    float a=0, b=0, c=0;
    std::cout << "\n\nWitaj w sprawdzaczu, czy z trzech odcinkow podanej przez Ciebie dlugosci da sie zbudowac trojkat!\n"
                 "Nie musza byc to wartosci calkowite, ale musza byc dodatnie.\n\n"
                 "Podaj kolejno dlugosci: pierwszego, drugiego i trzeciego odcinka:\n\n"
    "Pierwszy:\n\n";

    bool stop_a = false;
    while(!stop_a) {
        a=0;
        std::cin >> a;
        if (a < 0) {std::cout << "\n\nPrzeciez mowilem, ze dodatnie! Ehhh... Jeszcze raz podaj dlugosc pierwszego odcinka:\n\n";}
        else {stop_a = true;}
    }

    std::cout << "\n\nDrugi:\n\n";

    bool stop_b = false;
    while(!stop_b) {
        b=0;
        std::cin >>  b;
        if (b<0) {std::cout << "\n\nPrzeciez mowilem, ze dodatnie! Ehhh... Jeszcze raz podaj dlugosc drugiego odcinka:\n\n";}
        else {stop_b = true;}
    }

    std::cout << "\n\nTrzeci:\n\n";

    bool stop_c = false;
    while(!stop_c) {
        c=0;
        std::cin >> c;
        if (c < 0) {std::cout << "\n\nPrzeciez mowilem, ze dodatnie! Ehhh... Jeszcze raz podaj dlugosc trzeciego odcinka:\n\n";}
        else {stop_c = true;}
    }

    if (a+b < c || a+c < b || b+c < a) {std::cout << "\n\nZ podanych odcinkow nie da sie zbudowac trojkata. Bez odbioru.\n\n";}
    else {std::cout << "\n\nZ podanych odcinkow da sie zbudowac trojkat. Bez odbioru.\n\n";}
}

int main() {
    bool stop1 = false;
    while (!stop1) {
        char wybor = '0';
        std::cout<<"Witaj w przegladzie zadan z laboratorium nr 1! Wybierz nr zadania, ktorego dzialanie chcesz sprawdzic"
                "\nalbo wyjdz z programu wpisujac male \"x\":\n\n";
        std::cin >> wybor;
        switch (wybor) {
            case '1': {zadanie_1(); break;}
            case '2': {zadanie_2(); break;}
            case '3': {zadanie_3(); break;}
            case 'x': {stop1 = true; break;}
            default: {std::cout << "\n\nNie wybrano zadnej opcji. Restartuje...\n\n"; break;}
        }
    }
    return 0;
}
