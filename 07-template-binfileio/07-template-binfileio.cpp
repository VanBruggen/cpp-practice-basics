#include <iostream>
#include <cmath>
#include <vector>
#include <random>
#include <fstream>

#include "myComplex.hpp"

using int_Vector = std::vector <int>;
using float_Vector = std::vector <float>;

/*

*/

//======================================================================================================================

void AbyKontynuowac() {

    std::cout << "\n\n\nAby kontynuowac, wprowadz \";\" (srednik).\n\n";

    char c = 'a';

    do {
        c = getchar();
    } while (c != ';');

    std::cout << "\n\n";

}



std::fstream plik;

template <typename T>
void printVector_vertical(const std::vector<T>& v) {

    std::cout << "\n\n";

    for (auto e : v) {

        std::cout << e << "\n";
    }

    std::cout << "\n";
}

template <typename T>
void printVector_horizontal(const std::vector<T>& v) {

    for (auto e : v) {

        std::cout << e << "\t";
    }
}

int randomInt(const int& min, const int& max) {
    static std::default_random_engine e{};
    std::uniform_int_distribution <int> d(min, max);
    return d(e);
}

std::vector<int> randomIntVector (const unsigned int& size, const int& min, const int& max) {

    std::vector<int> v (size);

    for (int i=0; i < size; ++i) {

        v[i] = randomInt(min, max);
    }

    return v;
}

std::vector<float> randomFloatVector (const unsigned int& size) {

    std::vector<float> v(size);

    for (unsigned short i=0; i < size; ++i) {

        v[i] = randomFloat();
    }

    return v;
}

template <typename T>
void writeVector_text (const std::vector<T>& v) {

    plik.open("D:/zadania_na_PP/Laboratorium_7/tekst.txt", std::ios::out | std::ios::trunc);

    if ( plik.is_open() && plik.good() ) {

        for (int i = 0; i + 1 < v.size(); ++i) {

            plik << v[i] << "\t";
        }

        plik << v[v.size() - 1];

        plik.close();
        std::cout << "\n\nOtworz plik w notatniku i sprawdz, czy wszystko gra i trabi :)";

    } else {

        plik.close();
        std::cout << "\n\nNie udalo sie otworzyc pliku... Zrestartuj program i sprobuj ponownie.";
    }
}

template <typename T>
void writeVector_binary (const std::vector<T>& v) {

    plik.open("D:/zadania_na_PP/Laboratorium_7/binarka.bin", std::ios::out | std::ios::trunc | std::ios::binary);

    if ( plik.is_open() && plik.good() ) {

        for (int i = 0; i < v.size(); ++i) {

            plik.write((char*)(&v[i]), sizeof(v[i]));
        }

        plik.close();
        std::cout << "\n\nOtworz plik w edytorze binarek i sprawdz, czy wszystko gra i trabi :)";

    } else {

        plik.close();
        std::cout << "\n\nNie udalo sie otworzyc pliku... Zrestartuj program i sprobuj ponownie.";
    }
}



/* Zadanie 1. */

void zadanie_1() {

    unsigned short n;

    std::cout << "\n\nZadanie 1.\n\n"
                 "Podaj rozmiar wektora do zapisania:\n\n";

    std::cin.ignore();
    std::cin >> n;

    std::vector<float> v1 = randomFloatVector(n);

    writeVector_text(v1);

    writeVector_binary(v1);

    AbyKontynuowac();
}

/*
    Plik tekstowy ma 46 bajtów (po 1 bajcie na każdy znak w tekście [łącznie z białymi]), a binarny 20 bajtów,
    jako że nie przechowuje danych zrzutowanych na ciąg znaków, tylko bezpośrednie wartości zmiennych. Tych było 5,
    każda po 4 bajty, więc daje nam to 20 bajtów.

    "tekst.txt" otwierany notatnikiem ukazuje dokładnie to, co powinien, czyli wartości pięciu zmiennych zapisane
    jako ciągi znaków alfanumerycznych ("pod nimi" kryją się oczywiście inne wartości, niż te zmiennych).

    Binarka otwarta w notatniku pokazuje bezsensowny zbiór znaków, niepowiązanych ze sobą w żaden sposób. Wynika to
    z błędnej interpretacji surowych danych przez notatnik. Natywnie zamienia on każdy bajt na znak, a w tym pliku
    nie powinno się rozpatrywać pojedynczych bajtów, tylko 4 naraz i interpretować jako wartość zmiennoprzecinkową.
*/



/* Zadanie 2. */

template <typename T>
std::vector<T> readVector_binary() {

    plik.open("D:/zadania_na_PP/Laboratorium_7/binarka.bin", std::ios::in | std::ios::binary);

    std::vector<T> v (0);

    if ( plik.is_open() && plik.good() ) {

        while(true) {

            T temp;

            plik.read( (char*)(&temp), sizeof(temp) );

            if (plik.eof()) {break;}

            v.push_back(temp);
        }

        plik.close();
        return v;

    } else {

        plik.close();
        std::cout << "\n\nNie udalo sie otworzyc pliku... Zrestartuj program i sprobuj ponownie.";
        v.push_back(T(0));
        return v;
    }
}

void zadanie_2() {

    std::cout << "\n\nZadanie 2.\n\n\n"
                 "Wektor liczb calkowitych:\t";
    printVector_horizontal(readVector_binary<int>());

    std::cout << "\n\nWektor liczb zmiennoprzecinkowych:\t";
    printVector_horizontal(readVector_binary<float>());

    AbyKontynuowac();
}

/*
    Przy wczytaniu binarnie pliku "tekst.txt", wektory zostały oczywiście napełnione, ale wartościami, które z tekstem
    nie mają nic wspólnego. Przyczyną jest ponownie błędna interpretacja danych binarnych. Powinno się w tym przypadku
    interpretować po jednym bajcie jako znaki, a nie po 4 bajty jako l. całkowite albo wymierne. W zależności od
    funkcji, program powinien odpowiednio interpretować wczytywane dane, a to jak ma je interpretować zależy właśnie
    od programisty.
*/




/* Zadanie 3. */

void zadanie_3() {

    std::cout << "\n\nZadanie 3.\n\n";

    complex c1 = complex(4, 4);
    complex c2 = complex(4, 4);

    std::cout << "Pierwsza liczba:\n";
    printComplex(c1);

    std::cout << "\n\nDruga liczba:\n";
    printComplex(c2);

    std::cout << "\n\n\nDodawanie:\n";
    printComplex(add(c1,c2));

    std::cout << "\n\nOdejmowanie:\n";
    printComplex(subtract(c1,c2));

    std::cout << "\n\nMnozenie:\n";
    printComplex(multiply(c1,c2));

    std::cout << "\n\nPorownywanie:\nCzy c1 = c2 ?:\t" << equals(c1,c2);

    AbyKontynuowac();
}



/* Zadanie 4. */

void zadanie_4() {

    std::cout << "\n\nZadanie 4.\n\n";

    std::vector<complex> vC (0);

    for (int i=0; i < 10; ++i) {

        vC.push_back(randomComplex());
    }

    for (int i=0; i < vC.size(); ++i) {

        std::cout << "\n";
        printComplex(vC[i]);
    }

    writeComplexVector_binary(vC);

    AbyKontynuowac();
}

/*
    Wszystko gra i trąbi. Sprawdziłem w hexEditorze. Przy okazji, wychodzi na to, że przy zapisie obiektu struktury
    binarnie, program zapisuje po kolei pola, zgodnie z kolejnością i rozmiarem każdej z nich.
*/



/* Zadanie 5. */

void zadanie_5() {

    std::cout << "\n\nZadanie 5.\n\n";

    auto v = readComplexVector_binary();

    for (auto e : v) {

        std::cout << "\n";
        printComplex(e);
    }

    AbyKontynuowac();
}

/*
    Tu również wszystko gra i trąbi :)
*/



int main() {

    bool stop_menu = false;
    while (!stop_menu) {

        char wybor = 'a';

        std::cout << "\n\nWitaj w przegladzie zadan z laboratorium 7.! Lista opcji:\n\n"
                "\t\"1\" - Zadanie 1.\n"
                "\t\"2\" - Zadanie 2.\n"
                "\t\"3\" - Zadanie 3.\n"
                "\t\"4\" - Zadanie 4.\n"
                "\t\"5\" - Zadanie 5.\n"
                "\t\";\" (srednik) - Wyjdz z programu.\n\n\t";

        std::cin >> wybor;
        switch (wybor) {
            case '1': {zadanie_1(); break;}
            case '2': {zadanie_2(); break;}
            case '3': {zadanie_3(); break;}
            case '4': {zadanie_4(); break;}
            case '5': {zadanie_5(); break;}
            case ';': {stop_menu = true; break;}
            default: {std::cout << "\n\nNie wybrano zadnej opcji. Restartuje..."; break;}
        }
    }

    return 0;
}