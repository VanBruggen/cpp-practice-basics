#include "myComplex.hpp"



complex add(const complex& c1, const complex& c2) {

    return complex(c1.r + c2.r, c1.i + c2.i);
}

complex subtract(const complex& c1, const complex& c2) {

    return complex(c1.r - c2.r, c1.i - c2.i);
}

complex multiply(const complex& c1, const complex& c2) {

    return complex( (c1.r * c2.r) - (c1.i * c2.i), (c1.r * c2.i) + (c2.r * c1.i) );
}

bool equals(const complex& c1, const complex& c2) {

    return ( (c1.r == c2.r) && (c1.i == c2.i) );
}

void printComplex(const complex& c) {

    std::cout << "\nCzesc rzeczywista:\t" << c.r
              << "\nCzesc urojona:\t" << c.i
              << "\n";
}

float randomFloat() {

    static std::default_random_engine e{};
    std::uniform_real_distribution<float > d;
    return d(e);
}

complex randomComplex() {

    complex c = complex(randomFloat(), randomFloat());

    return c;
}

std::fstream plik1;

void writeComplexVector_binary (const std::vector<complex>& vC) {

    plik1.open("D:/zadania_na_PP/Laboratorium_7/binarka.bin", std::ios::out | std::ios::trunc | std::ios::binary);

    if ( plik1.is_open() && plik1.good() ) {

        for (int i = 0; i < vC.size(); ++i) {

            plik1.write( (char*)( &vC[i] ), sizeof( vC[i] ) );
        }

        plik1.close();
        std::cout << "\n\nOtworz plik w edytorze binarek i sprawdz, czy wszystko gra i trabi :)";

    } else {

        plik1.close();
        std::cout << "\n\nNie udalo sie otworzyc pliku... Zrestartuj program i sprobuj ponownie.";
    }
}

std::vector<complex> readComplexVector_binary() {

    plik1.open("D:/zadania_na_PP/Laboratorium_7/binarka.bin", std::ios::in | std::ios::binary);

    std::vector<complex> vC (0);

    if ( plik1.is_open() && plik1.good() ) {

        while(true) {

            complex temp;

            plik1.read( (char*)(&temp), sizeof(temp) );

            if (plik1.eof()) {break;}

            vC.push_back(temp);
        }

        plik1.close();
        return vC;

    } else {

        plik1.close();
        std::cout << "\n\nNie udalo sie otworzyc pliku... Zrestartuj program i sprobuj ponownie.";
        vC.push_back(complex());
        return vC;
    }
}