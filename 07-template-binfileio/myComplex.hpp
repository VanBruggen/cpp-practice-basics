#ifndef LABORATORIUM_7_MYCOMPLEX_HPP
#define LABORATORIUM_7_MYCOMPLEX_HPP


#include <iostream>
#include <fstream>
#include <vector>
#include <random>



struct complex {

    float r;
    float i;

    complex() {
        r = 0.0;
        i = 0.0;
    }

    complex(float x, float y) {

        this->r = x;
        this->i = y;
    }
};

complex add(const complex& c1, const complex& c2);

complex subtract(const complex& c1, const complex& c2);

complex multiply(const complex& c1, const complex& c2);

bool equals(const complex& c1, const complex& c2);

void printComplex(const complex& c);

float randomFloat();

complex randomComplex();

void writeComplexVector_binary (const std::vector<complex>& vC);

std::vector<complex> readComplexVector_binary();



#endif /* LABORATORIUM_7_MYCOMPLEX_HPP */